# Summary

[前導](./index.md)

+ [耕作篇](./farm.md)
+ [動物篇](./animals.md)
+ [寵物篇](./pet.md)
+ [釣魚篇](./fishing.md)
+ [工具篇](./tools.md)
+ [堀礦篇](./mining.md)
+ [戀愛篇](./romance.md)
+ [戀愛事件](./romance_2.md)
+ [賺錢篇](./money.md)

+ [GBA](./gba.md)
    + [懺悔效果介紹](./chapter_1.md)
    + [全種子介紹一覽](./chapter_2.md)
    + [真實之玉獲得方法介紹](./chapter_3.md)
    + [全居民人物生日及喜好物品一覽](./chapter_4.md)
    + [食譜大全料理配方製作一覽](./chapter_5.md)
    + [商店+營業時間大全](./chapter_6.md)
    + [力之果實獲得方法](./chapter_7.md)
    + [釣竿獲取方式與釣魚出現時間位置](./chapter_8.md)
    + [全工具作用介紹](./chapter_9.md)

___