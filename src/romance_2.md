# 戀愛事件

## 瑪麗戀愛事件

### 相遇 灰心0
星期二~日 天氣無要求\
AM10：00-PM4:00 圖書館 1 樓\
有煩惱可以說出來 -> 愛情度+3000\
很危險，以後要小心 -> 愛情度無變化\

### 友情事件1 灰心5000以上
星期二四六日 天氣無要求\
AM10：00-PM4:00 圖書館一樓\
一起找 -> 愛情度+3000\
拒絕 -> 愛情度-2000\

### 友情事件2 紫心10000以上
星期一 天氣晴\
PM12:00-PM5：00 山頂\
有大背包或大整理棚背包裡有空位\
試過 -> 愛情度+3000\
沒試過 -> 愛情度-2000\

### 戀人事件1 黃心40000以上
星期一 天氣晴\
AM10：00-PM4：00主人公的牧場\
瑪麗選擇 -> 愛情度+3000\
完全沒有興趣 -> 愛情度-2000\

### 戀人事件2 橙心50000以上
星期二~星期日 天氣晴\
PM12:00-PM5：00 圖書館 1 樓\
這是個好故事 -> 愛情度+3000\
…這是個沉重的故事 -> 愛情度-2000\

### 友情事件3 藍心20000以上
星期一 天氣無要求\
AM10:00-PM1：00 瑪麗家一樓\
沒什麼好提議 -> 愛情度-2000 （巴基爾、安娜好感度-10）\
牧場工作什麼的？ -> 愛情度+3000 （巴基爾、安娜好感度+20）\
巴基爾的工作？ -> 愛情度無變化（（巴基爾好感度+20）\

## 卡蓮戀愛事件

### 相遇 愛情度0
星期三五 天氣無要求\
AM10：00-PM1:00 雜貨屋\
裝備欄有空位\
說真的，有點困難… -> 愛情度+3000 （傑夫好感度T0,沙夏好感度+20）\
沒問題 -> 愛情度無變化（傑夫、沙夏好感度+20）\

### 友情事件1 灰心5000以上
星期一四六 天氣無要求\
AM10:00-PM1:00 雜貨屋\
背包裡有空位\
月淚草種子 -> 愛情度+3000 （傑夫好感度+20、沙夏好感度-10）\
貓薄荷種子 -> 愛情度-2000 （傑夫、沙夏好感度+20）\

### 友情事件2 紫心10000以上
星期三五 天氣無要求\
AM10：00-PM1:00 雜貨屋\
有大背包或大整理棚背包裡有空位\
陪著一起去 -> 愛情度+3000 （笛克、傑夫、沙夏好感度+20）\
還有別的事 -> 愛情度-2000 （笛克、傑夫、沙夏好感度-10）\

### 友情事件3 藍心20000以上
星期一四六 天氣無要求\
AM10:00-PM1:00 雜貨屋\
背包裡有空位\
吃 -> 愛情度+3000 （傑夫、沙夏好感度+20）\
不吃 -> 愛情度-2000 （傑夫、沙夏好感度-10）\

### 戀人事件1 黃心40000以上
星期一四六 天氣晴\
AM10：00-PM1:00 雜貨屋\
【男主人公】\
希望你多依靠戀人一些 -> 愛情度+3000\
因為我擔心卡蓮 -> 愛情度-2000\
【女主人公】\
希望你多依靠摯友一些 -> 愛情度+3000\
因為我擔心卡蓮 -> 愛情度-2000\

### 戀人事件2 橙心50000以上
日期不固定 天氣晴\
AM10：00-PM3：00森林（主人公牧場的南方向）\
你想太多了 -> 愛情度-2000\
讓人意外的一面 -> 愛情度+3000\

## 珍妮佛戀愛事件

### 相遇 愛情度0
星期一~六 天氣晴\
AM10:00-PM12:00 後山\
你在說些什麼… -> 愛情度-2000\
嗯，感覺很舒服 -> 愛情度+3000\

### 友情事件1 灰心5000以上
星期二~六 天氣晴\
AM10：00-PM12:00 後山\
和哈里斯不是第一次見面\
思考方式不同 -> 愛情度-2000\
很棒的解釋 -> 愛情度+3000\

### 友情事件2 紫心10000以上
星期一~六 天氣晴\
AM10:00-PM12:00 後山\
像天使的名字 -> 愛情度+3000\
像魔法的名字 -> 愛情度無變化\
總之取個高貴的名字 -> 愛情度-2000\

### 友情事件3 藍心20000以上
星期一~六 天氣無要求\
AM11：00-PM4:00 雞屋［樓有大背包整理棚背包裡有空位\
不是天然的 -> 愛情度-2000 （莉莉婭好感度-10）\
真溫柔啊 -> 愛情度+3000 （莉莉婭好感度+20）\

### 戀人事件1 黃心40000以上
日期不固定 天氣晴\
AM10:00-PM4：00 後山帳篷\
父母一定很擔心你-> 愛情度-2000\
自己的幸福才是最重要的 -> 愛情度+3000\

### 戀人事件2 橙心50000以上
日期不固定 天氣無要求\
PM7:00-PM9：00 宿屋2樓\
這很難說… -> 愛情度-2000\
別太擔心 -> 愛情度+3000\

## 蘭愛情事件

### 相遇 灰心0
星期二五~日 天氣無要求\
AM1O:OO-PM1:OO 宿屋 1 樓\
吃 -> 愛情度+3000 （達特好感度+20）\
不用麻煩了 -> 愛情度-2000 （達特好感度-10）\

### 友情事件1 灰心5000以上
星期二四六日 天氣無要求\
AM10:00-PMl:00 宿屋2樓\
喜歡打掃 -> 愛情度+3000\
討厭打掃 -> 愛情度-2000\

### 友情事件2 紫心10000以上
星期一五 天氣無要求\
PM12:00-PM7:00 宿屋［樓\
有大背包或大整理棚背包裡有空位\
送到醫院 -> 愛情度+3000 （多特、艾麗、達特好感度+20）\
怎•••怎麼辦好呢… -> 愛情度無變化\

### 友情事件3 藍心20000以上
星期二四六日 天氣無要求\
PM12:00-PM7:00 宿屋 1 樓\
【第一個選擇】\
是的，我喜歡她 -> 愛情度無變化（達特好感度+20）\
只是朋友而已 -> 愛情度無變化（達特好感度-10）\
【第二個選擇】\
大大咧咧 -> 愛情度-2000\
是個好父親 -> 愛情度+3000\

### 戀人事件1 黃心40000以上
星期一三~六 天氣晴\
AM9:00-PM5：00 宿屋 1 樓\
找找強效除漬劑？ -> 愛情度-2000\
轉換一下心情吧 -> 愛情度+3000\

### 戀人事件2 橙心50000以上
星期二~日 天氣無要求\
AM10：00-PM4:00 圖書館 1 樓\
真像蘭會做的事 -> 愛情度+M000\
你不擅長學習 -> 愛情度-2000\

## 珀布莉戀愛事件

### 相遇 灰心0
星期三五 天氣晴\
AM11:20-PM6:00城鎮南部（養雞場附近）\
背包裡有空位\
喜歡 -> 愛情度+3000\
討厭 -> 愛情度-2000\

### 友情事件1 灰心5000以上
星期一~星期六 天氣晴\
AM6：00-PM12:00主人公的牧場\
背包裡有空位\
不要我很忙！ -> 愛情度-2000\
好啊 -> 愛情度+3000\

### 友情事件2 紫心10000以上
星期日 天氣晴\
有大背包或大整理棚背包裡有空位\
AM10：00-PM1:00 教堂\
一起玩 -> 愛情度+3000 （梅、卡特好感度+20,優好感度-10）\
我還有事 -> 愛情度無變化（梅友好度+20,優、卡特好感都-10）\

### 友情事件3 藍心20000以上
星期一三~六 天氣晴\
AM11：20-PM1:00 雞屋 1 樓\
背包裡有空位\
珀布莉是正確的 -> 愛情度+3000 （裡克好感度-10,莉莉婭好感度+20）\
裡克是正確的 -> 愛情度-2000 （裡克好感度+20,莉莉婭好感度-10）\
莉莉婭很困擾 -> 愛情度+3000 （裡克、莉莉婭好感度+20）\

### 戀人事件1 黃心40000以上
日期不固定 天氣晴\
AM10:00-PMl:00主人公的牧場\
背包裡有空位\
珀布莉的手法意外地好 -> 愛情度+3000\
不安 -> 愛情度-2000\

### 戀人事件2 橙心50000以上
星期一~三五~日 天氣晴\
AM10：00-PMl:00 城鎮南\
讓我困擾你很開心嗎？ -> 愛情度+3000\
夠了沒! -> 愛情度-2000\

## 艾麗愛情事件

### 相遇 灰心0
星期二四~日 天氣無要求\
AM9：00-PM7:00 醫院 1 樓\
嗯，理解 -> 愛情度-2000 （多特、優好感度+20）\
做弄別人不好 -> 愛情度+3000 （優好感度-10）\

### 友情事件1 灰心5000以上
星期四六日 天氣無要求\
AM9：00-PM7:00 醫院］樓\
跟傑夫不是第一次見面\
下次小注意點 -> 愛情度+3000\
…… -> 愛情度-2000\

### 友情事件2 紫心10000以上
星期三 天氣晴\
AM9：00-PM1:00 艾麗家\
和艾蓮奶奶不是第一次見面\
有大背包或整理棚背包裡有空位\
拒絕 -> 愛情度-2000 （優好感度-10）\
答應 -> 愛情度+3000 （優、艾蓮好感度+20）\

### 友情事件3 藍心20000以上
星期三 天氣晴\
AM9:00-PM6：00 海岸\
是慣壞了 -> 愛情度-2000\
沒那回事 -> 愛情度+3000\

### 戀人事件1 黃心40000以上
星期三 天氣晴\
AM9:00-PMl:00 艾麗家\
跟梅不是第一次見面\
一定會變成好孩子 -> 愛情度+3000\
有點擔心… -> 愛情度-2000\

### 戀人事件2 橙心50000以上
星期三 天氣晴\
AM9:00-PM1：00 艾麗家\
我也要謝謝你 -> 愛情度+3000\
希望實際一點的感謝 -> 愛情度-2000\

