# 挖礦技巧彙總

1. 首先是老玩家和新人都有的誤區是挖礦很累。
在舊版中因為背包只能帶8個回覆道具，所以SL大法是必要的不然絕對不可能255層。
在新版最加強版回覆藥水100體力回覆，一組9個 你可以最多帶24組，真的直接下礦吧，詛咒上一層保存一下就可哥以。
沒必要SL大法。你會發現255後為什麼我還剩這麼多回覆藥。

1. 誤區避開陷阱，新版中加入了陷阱機制，你踩到陷阱會跳下去跳躍層數1-9層 並減少大量體力，所以很多人不敢踩陷阱。
其實與其說陷阱是陷阱不如說它是電梯，讓你更快的到達目標礦層，而且你有最多24組回覆藥水，摔死算我的。

1. 誤區只帶回覆藥水不帶疲勞藥水，回覆藥水回覆體力，疲勞藥水是回覆疲勞，疲勞數值就是體力旁邊的表情，
在體力0的情況下疲勞滿的是不會暈倒的，只有疲勞0才會暈倒，最好的組合是1組疲勞藥水，和23組回覆藥水，
當然實際是10多組回覆藥水以及足夠了。

1. 誤區河童秘寶和女神秘寶都要挖到。
我個人是沒有挖河童秘寶，因為它只回覆疲勞值，但實際上疲勞值是很難到 0 的在吃了藍色果實之後。
所以我個人只推薦挖到女神秘寶，女神秘寶裝備後就和在溫泉裡一樣的回覆速度，還蠻實用的，如果裝備女神秘寶後再去溫泉回覆速度兩倍。

1. 誤區冬天后就能挖到詛咒道具，要挖到詛咒一點要全道具秘銀，才能挖到詛咒，
不知道這一點怎麼刷也刷不到詛咒的，不要浪費你的精力。

1. 誤區挖到詛咒後就可以挖到賢者之石了。
賢者之石第二年春天並且所有詛咒變成祝福才能挖到。

1. 誤區泉邊礦場255層有飛行石頭，有是有但是要三年後。
